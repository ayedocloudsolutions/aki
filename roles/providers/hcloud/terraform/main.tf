provider "hcloud" {
  token = var.hcloud_token
}

# Shared tags
locals {
  # Common tags to be assigned to all resources
  common_tags = {
    "release" = var.stack
    "provider" = "aki"
  }

  all_tags = merge(local.common_tags)
}

output "inventory" {
  value = templatefile("${path.module}/templates/inventory.tpl", { masters = hcloud_server.master, nodes = hcloud_server.node })
}

resource "local_file" "inventory"{
  content = templatefile("${path.module}/templates/inventory.tpl", { masters = hcloud_server.master, nodes = hcloud_server.node })
  filename = "${path.module}/inventory.yml"
}

resource "hcloud_ssh_key" "ssh_key" {
  name       = join("-",[var.stack, "ssh"])
  public_key = file(var.ssh_public_key)
}

resource "hcloud_server" "master" {
  count = var.master_count
  name = join("-",[var.stack,"master",count.index])
  image = var.os_image
  server_type = var.master_type
  ssh_keys  = [hcloud_ssh_key.ssh_key.id]
  labels = local.all_tags
  location = var.master_location
}

resource "hcloud_server" "node" {
  count = var.node_count
  name = join("-",[var.stack,"node",count.index])
  image = var.os_image
  server_type = var.node_type
  ssh_keys  = [hcloud_ssh_key.ssh_key.id]
  labels = local.all_tags
  location = var.node_location
}
