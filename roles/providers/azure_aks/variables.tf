# Author: Antony Goetzschel <ago@ccsolutions.io>
# Date: 10.11.20
# Terraform Version: 0.13.2

variable "tenant_id" {
  description = "Tenant ID"
  type    = string
  default = "539c9603-6380-423c-a70c-01acd69c91d5"
}

variable "subscription_id" {
  description = "Subscription ID"
  type    = string
  default = "38346067-832c-4732-b232-a169ea63bfd4"
}

variable "resource_group" {
  description = "Location of the resources"
  type    = string
  default = "Hornbach_KI"
}

variable "location" {
  description = "Location of the resources"
  type    = string
  default = "westeurope"
}