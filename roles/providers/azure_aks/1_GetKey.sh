#!/bin/bash
ENVFILE=.env
KEY=$(az keyvault secret show --name "terraform-backend-key" --vault-name "kvterraformstateskey" --query value -o tsv --subscription 38346067-832c-4732-b232-a169ea63bfd4)

#//TODO ADD AZ LOGIN
if test -f "$ENVFILE"; then
    echo "--> $ENVFILE exists."
    echo "Please check if the content of the file is:"
    echo ""
    echo "#!/bin/bash"
    echo "export ARM_ACCESS_KEY=$(echo $KEY)"
    echo ""
    echo "---> or delete $ENVFILE and re run the script"
    echo "---> if the .env file has the same content pelase run: "
    echo ""
    echo "source $ENVFILE"
    exit 0
else
    echo "---> $ENVFILE not exists"
    echo "---> Creating $ENVFILE"
    touch $ENVFILE
    echo "#!/bin/bash" >> $ENVFILE
    echo "export ARM_ACCESS_KEY=$(echo $KEY)" >> $ENVFILE
    echo "---> File $ENVFILE created"
    echo "---> Please run:"
    echo "source $ENVFILE"
    echo ""
    echo "---> H A P P Y  T E R R A F O R M I N G"
    echo "---> !!! PLEASE BE CAREFUL !!!"
    chmod 755 $ENVFILE
    exit 0
fi