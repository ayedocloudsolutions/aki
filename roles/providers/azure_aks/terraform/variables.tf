variable "client_id" {
  description = "Azure Kubernetes Service Cluster service principal"
}

variable "client_secret" {
  description = "Azure Kubernetes Service Cluster password"
}

variable "stack" {}

variable "resource_group" {}

variable "node_count" {
    default = 3
}

variable "node_size" {
    default = "Standard_D2_v2"
}

variable "location" {
    default = "Germany West Central"
}

