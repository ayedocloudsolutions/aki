# [1.7.0](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.6.2...v1.7.0) (2021-08-23)


### Features

* added azure aks integration ([2292964](https://gitlab.com/ayedocloudsolutions/aki/commit/2292964803687fb2fa7d3b95d22dc828407ee7ea))

## [1.6.2](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.6.1...v1.6.2) (2021-08-12)


### Bug Fixes

* ssh usage ([d01ac5a](https://gitlab.com/ayedocloudsolutions/aki/commit/d01ac5a22a75fae0596a869489a18a1304445cd7))

## [1.6.1](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.6.0...v1.6.1) (2021-08-12)


### Bug Fixes

* private key file ([8c3026f](https://gitlab.com/ayedocloudsolutions/aki/commit/8c3026fbecc41842c6bb0c66546e5d37bc012010))
* private key file ([bbf40ec](https://gitlab.com/ayedocloudsolutions/aki/commit/bbf40ecd3cd2cda3a45f293dbba0e7ba95003b84))

# [1.6.0](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.5.0...v1.6.0) (2021-08-12)


### Features

* restructured ssh config ([edd83bc](https://gitlab.com/ayedocloudsolutions/aki/commit/edd83bc8ee8281c6aaf7e946a9b99ebe47d66575))

# [1.5.0](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.4.1...v1.5.0) (2021-08-12)


### Features

* docker ([e649dd0](https://gitlab.com/ayedocloudsolutions/aki/commit/e649dd0908ad85e81be4248affe1d744d77bf39b))

## [1.4.1](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.4.0...v1.4.1) (2021-08-11)


### Bug Fixes

* removed clutter ([4a5023a](https://gitlab.com/ayedocloudsolutions/aki/commit/4a5023a4800248b8bafeecdda2e83ae89433bbee))

# [1.4.0](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.3.2...v1.4.0) (2021-08-11)


### Bug Fixes

* stack name variable ([1e205e1](https://gitlab.com/ayedocloudsolutions/aki/commit/1e205e16822a14e60640f05e7e810253ca22a7d0))


### Features

* added Earthfile ([3b85e30](https://gitlab.com/ayedocloudsolutions/aki/commit/3b85e30e52b85ca2469ef7796d5f57a08d90e28d))

## [1.3.2](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.3.1...v1.3.2) (2021-08-06)


### Bug Fixes

* added check for availability of stack.name ([a72b189](https://gitlab.com/ayedocloudsolutions/aki/commit/a72b1895c823b361294b7527b323965583966661))

## [1.3.1](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.3.0...v1.3.1) (2021-07-30)


### Bug Fixes

* install.yml ([a4b4a6f](https://gitlab.com/ayedocloudsolutions/aki/commit/a4b4a6f16c16414f772f3e83ee2af2f1a14064fd))

# [1.3.0](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.2.0...v1.3.0) (2021-07-30)


### Features

* enabled usage of acs.yml and acs.yaml, closes [#10](https://gitlab.com/ayedocloudsolutions/aki/issues/10) ([b2198b3](https://gitlab.com/ayedocloudsolutions/aki/commit/b2198b34fdcc1ee9dc8848c61fc8a79c0f44894b))

# [1.2.0](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.1.1...v1.2.0) (2021-07-30)


### Bug Fixes

* moved stack_dir one up ([da30934](https://gitlab.com/ayedocloudsolutions/aki/commit/da30934771c7805cb0ff0885204cb6007329e81a))


### Features

* added new config file in /acs.yml ([6e7232b](https://gitlab.com/ayedocloudsolutions/aki/commit/6e7232b891963088c972918adbe20ef48ba5d635))

## [1.1.1](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.1.0...v1.1.1) (2021-07-30)


### Bug Fixes

* renamed release to stack closes [#12](https://gitlab.com/ayedocloudsolutions/aki/issues/12) ([c8627c6](https://gitlab.com/ayedocloudsolutions/aki/commit/c8627c6bff6937cf8b5c2145fa297aa4bff51b19))

# [1.1.0](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.0.6...v1.1.0) (2021-06-25)


### Features

* added diagram for AKI ([7a023fc](https://gitlab.com/ayedocloudsolutions/aki/commit/7a023fc9c32116211d6ece76ce52ec4c1d81b288))

## [1.0.6](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.0.5...v1.0.6) (2021-06-13)


### Bug Fixes

* using acs-base-image in Dockerfile ([b0051e6](https://gitlab.com/ayedocloudsolutions/aki/commit/b0051e62d2799618e6ef07affd02b4eb19d19af8))

## [1.0.5](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.0.4...v1.0.5) (2021-06-13)


### Bug Fixes

* table in docs was messed up ([32b9d63](https://gitlab.com/ayedocloudsolutions/aki/commit/32b9d6380334af8b69b884ba20db234a0381638a))

## [1.0.4](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.0.3...v1.0.4) (2021-06-13)


### Bug Fixes

* CI ([e4f661f](https://gitlab.com/ayedocloudsolutions/aki/commit/e4f661ff1c99cb3590c97b781cec640e905947bc))
* CI ([d58be67](https://gitlab.com/ayedocloudsolutions/aki/commit/d58be67fd29d94b9eb5bcb2f0582d2717315a42a))
* CI debugging ([7572859](https://gitlab.com/ayedocloudsolutions/aki/commit/7572859b202f965d62f611094563a91dc2d57081))
* CI debugging ([613b157](https://gitlab.com/ayedocloudsolutions/aki/commit/613b15729358becfad21a1cfe3c5b7b7635cb2aa))
* CI debugging ([20a00d2](https://gitlab.com/ayedocloudsolutions/aki/commit/20a00d26c2e10cccc7abe8a7380ba3b7ef946275))
* CI debugging ([f77c6d0](https://gitlab.com/ayedocloudsolutions/aki/commit/f77c6d0643ee0253e4f6c80bebad112678c05db8))

## [1.0.3](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.0.2...v1.0.3) (2021-06-13)


### Bug Fixes

* build docker image in ci ([30379b1](https://gitlab.com/ayedocloudsolutions/aki/commit/30379b157f4f990d0ac2fb6788d9207d39dddf47))

## [1.0.2](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.0.1...v1.0.2) (2021-06-13)


### Bug Fixes

* refactor config, fix uninstall routine, update docs ([035664c](https://gitlab.com/ayedocloudsolutions/aki/commit/035664c81e3049f67270ef473327a85317f1fad5))

## [1.0.1](https://gitlab.com/ayedocloudsolutions/aki/compare/v1.0.0...v1.0.1) (2021-06-02)


### Bug Fixes

* Variables ([e1014ae](https://gitlab.com/ayedocloudsolutions/aki/commit/e1014aeedf7e6f07f155a8b9f342515aabb9d737))

# 1.0.0 (2021-05-27)


### Bug Fixes

* added main as release branch ([a25f124](https://gitlab.com/ayedocloudsolutions/aki/commit/a25f124dce31e25413837d7c13ebea4ce9d55793))


### Features

* Launch ayedo Kubernetes Infrastructure ([283ccaf](https://gitlab.com/ayedocloudsolutions/aki/commit/283ccaf73c841e89ee211fee325b792720b57391))
